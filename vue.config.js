module.exports = {
    css: {
        loaderOptions: {
            less: {
                prependData: `@import "@/styles/_variables.less";`
            }
        }
    }
}