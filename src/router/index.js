import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'

Vue.use(VueRouter)

const Features = () => import('../views/Features.vue')

  const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },

  {
    path: '/features',
    name: 'Features',
    component: Features
  }
]

const router = new VueRouter({
  routes
})

export default router
