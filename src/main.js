import Vue from 'vue'
import App from './App.vue'
import router from './router'
import VueLazyload from 'vue-lazyload'

/**
 * Importing FontAwesome Icons
 */
import { library } from '@fortawesome/fontawesome-svg-core';
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome';
import { faBars } from "@fortawesome/free-solid-svg-icons";
import { faUikit, faLess, faVuejs, faFontAwesomeFlag } from "@fortawesome/free-brands-svg-icons";

library.add(faBars, faUikit, faLess, faVuejs, faFontAwesomeFlag);

Vue.use(VueLazyload);

Vue.component('font-awesome-icon', FontAwesomeIcon);

Vue.config.productionTip = false

new Vue({
  router,
  render: h => h(App)
}).$mount('#app')
